from flask import request
from marshmallow import Schema, ValidationError
from marshmallow.validate import Length, Range
from marshmallow.fields import (
    Integer,
    String,
    Float,
    List,
    Nested
)


def validate(func):
    def wrapper(*args, **kwargs):
        quote_schema = QuoteSchema()
        try:
            data = quote_schema.load(request.get_json())
            args = (*args, data)
            return func(*args, **kwargs)
        except ValidationError as err:
            return err.messages, 422
    return wrapper


class ItemSchema(Schema):
    id = Integer(required=True, error='Field name is required.')
    name = String(required=True, error='Field name is required.')


class QuoteSchema(Schema):
    name = String(required=True,
                  validate=Length(min=0),
                  error='Field name is required.')

    price = Float(required=True,
                  validate=Range(min=0),
                  error='Field price is required and most be higher then 0.')

    items = List(Nested(ItemSchema,
                 validate=Length(min=0)))
