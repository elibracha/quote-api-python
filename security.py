from models import UserModel
from werkzeug.security import safe_str_cmp


def authenticate(username, password):
    user = UserModel.get_by_name(username)
    if user and safe_str_cmp(user.password.encode('utf-8'), password.encode('utf-8')):
        return user


def identity(payload):
    user_id = payload['identity']
    return UserModel.get_by_id(user_id)
