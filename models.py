from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class QuoteModel(db.Model):
    global db

    quote_item = db.Table('quote_item', db.Model.metadata,
                          db.Column('quote_id', db.Integer, db.ForeignKey('quotes._id')),
                          db.Column('item_id', db.Integer, db.ForeignKey('items._id'))
                                 )
    __tablename__ = 'quotes'

    _id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    price = db.Column(db.Float(precision=3), nullable=False)
    items = db.relationship("ItemModel", secondary=quote_item)

    def __init__(self, name, price, items):
        self.name = name
        self.price = price

        for item in items:
            db_item = ItemModel.get_by_id(item['id'])
            self.items.append(db_item if db_item else ItemModel(**item))

    @classmethod
    def get_by_id(cls, _id):
        return cls.query.filter_by(_id=_id).first()

    @classmethod
    def get_by_name(cls, name):
        return cls.query.filter_by(name=name).first()

    def unique_name(self):
        if self.get_by_name(self.name):
            return False
        return True

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        self.items.clear()
        db.session.delete(self)
        db.session.commit()

    def json(self):
        return {
            "id": self._id,
            "name": self.name,
            "price": self.price,
            "items": [item.json() for item in self.items],
        }


class ItemModel(db.Model):
    global db

    __tablename__ = 'items'

    _id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)

    def __init__(self, id, name):
        self._id = id
        self.name = name

    @classmethod
    def get_by_id(cls, _id):
        return cls.query.filter_by(_id=_id).first()

    def json(self):
        return {
            "id": self._id,
            "name": self.name
        }


class UserModel(db.Model):
    global db

    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)

    def __init__(self, username, password):
        self.username = username
        self.password = password

    @classmethod
    def get_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def get_by_name(cls, username):
        return cls.query.filter_by(username=username).first()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def json(self):
        return {
            "id": self.id,
            "username": self.username,
            "password": self.password
        }