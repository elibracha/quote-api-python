import os

from flask import Flask
from flask_restful import Api
from models import db
from resources import Quote, QuoteList, User
from flask_jwt import JWT
from security import authenticate, identity

app = Flask(__name__)
api = Api(app)
jwt = JWT(app, authenticate, identity)

app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('CLEARDB_DATABASE_URL', 'sqlite:///:memory:')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_RECORD_QUERIES'] = False
app.config['SQLALCHEMY_ECHO'] = False
app.config['JWT_SECRET_KEY'] = os.environ.get('SECRET_KEY', 'super-secret')

api.add_resource(Quote,
                 '/quote/<string:_id>',
                 '/quote')

api.add_resource(QuoteList,
                 '/quotes')

api.add_resource(User,
                 '/user')


@app.before_first_request
def initialize():
    db.init_app(app)
    db.create_all()


if __name__ == '__main__':
    app.run(debug=True)
