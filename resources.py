from flask_restful import Resource
from models import QuoteModel
from schemas import validate
from flask_jwt import jwt_required
from flask import request
from models import UserModel


class Quote(Resource):
    def get(self, _id):
        quote = QuoteModel.get_by_id(_id)
        return quote.json(), 200 if quote else {'message': f'no quote with id {_id}'}, 400

    @jwt_required()
    def delete(self, _id):
        quote = QuoteModel.get_by_id(_id)
        if quote:
            quote.delete()
            return {'message': 'quote was deleted'}, 200
        else:
            return {'message': f'no quote with id {_id}'}, 400

    @validate
    def put(self, data, _id):
        quote = QuoteModel(**data)
        db_quote = QuoteModel.get_by_id(_id)
        if db_quote:
            db_quote.name = quote.name
            db_quote.price = quote.price
            db_quote.items = quote.items

            db_quote.save()

            return db_quote.json(), 200
        return {'message': f'no quote with id {_id}'}, 400

    @validate
    def post(self, data):
        quote = QuoteModel(**data)
        if quote.unique_name():
            quote.save()
            return quote.json(), 201
        return {'name': ['quote name most be unique.']}, 400


class QuoteList(Resource):
    def get(self):
        return {'items': list(map(lambda quote: quote.json(), QuoteModel.query.all()))}


class User(Resource):
    def post(self):
        json = request.get_json()
        user = UserModel(**json)
        user.save()
        return {'message': 'user created.'}, 200
